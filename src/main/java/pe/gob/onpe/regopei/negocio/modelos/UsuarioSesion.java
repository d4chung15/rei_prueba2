package pe.gob.onpe.regopei.negocio.modelos;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UsuarioSesion extends ModeloBase {

    private Integer idUsuarioSesion;
    private String codigoSession;
    //private Usuario usuario;

}
