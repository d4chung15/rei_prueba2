package pe.gob.onpe.regopei.negocio.servicios.implementacion.captcha;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import pe.gob.onpe.regopei.transversal.excepciones.ReCaptchaInvalidException;
import pe.gob.onpe.regopei.transversal.excepciones.ReCaptchaUnavailableException;
import pe.gob.onpe.regopei.vista.dtos.output.captcha.GoogleOutputDto;

import java.net.URI;

@Log4j2
@Service("captchaService")
public class CaptchaServicioImpl extends AbstractCaptchaServicio {
    @Override
    public void processResponse(final String response) {
        securityCheck(response);

        final URI verifyUri = URI.create(String.format(RECAPTCHA_URL_TEMPLATE, getReCaptchaSecret(), response, getClientIP()));
        try {
            final GoogleOutputDto googleResponse = restTemplate.getForObject(verifyUri, GoogleOutputDto.class);
            log.debug("Google's response: {} ", googleResponse.toString());

            if (!googleResponse.isSuccess()) {
                if (googleResponse.hasClientError()) {
                    reCaptchaAttemptService.reCaptchaFailed(getClientIP());
                }
                throw new ReCaptchaInvalidException("reCaptcha no se validó con éxito.");
            }
        } catch (RestClientException rce) {
            throw new ReCaptchaUnavailableException("Registro de reCaptcha no disponible en este momento. Por favor, inténtelo de nuevo más tarde", rce);
        }
        reCaptchaAttemptService.reCaptchaSucceeded(getClientIP());
    }
}
