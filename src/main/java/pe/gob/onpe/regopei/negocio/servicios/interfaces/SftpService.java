package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import java.io.InputStream;
import java.io.OutputStream;

public interface SftpService {
        boolean uploadFile(String path, InputStream fileInputStream);
        boolean removeFile(String path);
        boolean downloadFile(String path, OutputStream outputStream);
}
