package pe.gob.onpe.regopei.negocio.modelos;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GeneralInfoArchive extends ModeloBase {
    private Integer id;
    private Integer generalInfoId;
    private Archive archive;
    private Integer docId;
}
