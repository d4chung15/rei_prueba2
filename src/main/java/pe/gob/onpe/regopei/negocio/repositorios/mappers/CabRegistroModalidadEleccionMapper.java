package pe.gob.onpe.regopei.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.regopei.negocio.modelos.CabRegistroModalidadEleccion;



@Mapper
public interface CabRegistroModalidadEleccionMapper {
	
   void obtenerPorOopp(CabRegistroModalidadEleccion param) throws Exception;
   
   void guardarActualizar(CabRegistroModalidadEleccion param) throws Exception;
   
}
