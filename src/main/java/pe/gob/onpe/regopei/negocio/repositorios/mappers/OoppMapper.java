package pe.gob.onpe.regopei.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.regopei.negocio.modelos.Oopp;


@Mapper
public interface OoppMapper {
	
   void listarOoppPorUsuario(Oopp oopp) throws Exception;
   
   void obtenerOoppPorId(Oopp oopp) throws Exception;
   
}
