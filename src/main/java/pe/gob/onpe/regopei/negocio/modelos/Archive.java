package pe.gob.onpe.regopei.negocio.modelos;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Archive extends ModeloBase {
    private Integer id;
    private String guid;
    private String path;
    private String originalFilename;
    private String filename;
    private String format;
    private Long size;
}
