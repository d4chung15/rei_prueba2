package pe.gob.onpe.regopei.negocio.beans;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.regopei.negocio.modelos.ModeloBase;


@Getter
@Setter
public class Persona  extends ModeloBase{
	
	private String nombres;
	private String numeroDocumento;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String soloNombres;

}
