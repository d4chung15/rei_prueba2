package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.regopei.negocio.modelos.DistritoElectoral;
import pe.gob.onpe.regopei.negocio.repositorios.mappers.DistritoElectoralMapper;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.DistritoElectoralService;
import pe.gob.onpe.regopei.transversal.utilidades.Funciones;

@Service
public class DistritoElectoralServiceImpl implements DistritoElectoralService {

	@Autowired
	private DistritoElectoralMapper mapper;
	
	
	@Override
	public void obtenerDistritoElectoralModalidadDelegados(DistritoElectoral distritoElectoral) throws Exception {
		
		mapper.obtenerDistritoElectoralModalidadDelegados(distritoElectoral);
		
		//Se realiza la obtencion del Distrito Lima si la circunscripcion = 1 y padre 0 
		if( distritoElectoral.getCircunscripcion() != null && distritoElectoral.getCircunscripcion() == 1 && distritoElectoral.getIdPadre() != null && distritoElectoral.getIdPadre() == 0) {
			DistritoElectoral filtro = new DistritoElectoral();
			filtro.setId(15);
			filtro.setCircunscripcion(2);
			filtro.setIdCabRegistroModalidadEleccion(distritoElectoral.getIdCabRegistroModalidadEleccion());
			mapper.obtenerDistritoElectoralModalidadDelegados(filtro);
			
			if(filtro.getResultado() == 1 && !distritoElectoral.getLista().isEmpty()) {
				distritoElectoral.getLista().addAll(filtro.getLista());
				
			}
		}
		
		Funciones.validarOperacionConBaseDatos(distritoElectoral, DistritoElectoralServiceImpl.class, "obtenerDistritoElectoralModalidadDelegados");
		
	}

}
