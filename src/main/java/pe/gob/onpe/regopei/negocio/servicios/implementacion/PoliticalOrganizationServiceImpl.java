package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.regopei.negocio.modelos.*;
import pe.gob.onpe.regopei.negocio.repositorios.mappers.ArchiveMapper;
import pe.gob.onpe.regopei.negocio.repositorios.mappers.PoliticalOrganizationMapper;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.PoliticalOrganizationService;
import pe.gob.onpe.regopei.vista.dtos.input.POSaveArchiveInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.CentralElectoralBodyMemberDto;
import pe.gob.onpe.regopei.vista.dtos.output.CentralElectoralBodyMembersOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.POArchiveListOuputDto;

import java.util.ArrayList;
import java.util.List;

@Service
public class PoliticalOrganizationServiceImpl implements PoliticalOrganizationService {

    @Autowired
    private PoliticalOrganizationMapper representativeMapper;

    @Autowired
    private ArchiveMapper archiveMapper;

    @Override
    public PoliticalOrganization getPoliticalOrganization(Integer id) throws Exception {
        return representativeMapper.getPoliticalOrganization(id);
    }

    @Override
    public PORepresentative getPoliticalOrganizationInfo(Integer id) throws Exception {
        return representativeMapper.getPoliticalOrganizationGeneralInfo(id);
    }

    @Override
    public void savePoliticalOrganizationRepresentative(PORepresentative poRepresentative) throws Exception {
        representativeMapper.savePoliticalOrganizationRepresentative(poRepresentative);
    }

    @Override
    public CentralElectoralBodyMembersOutputDto listCElectoralMembers(Integer id) throws Exception {
        List<CElectoral> list = representativeMapper.listCElectoralByPOId(id);

        CentralElectoralBodyMembersOutputDto ans = new CentralElectoralBodyMembersOutputDto();
        ans.setMembers(list);
        return ans;
    }

    @Override
    public void saveCElectoralBodyMember(Integer poid, CentralElectoralBodyMemberDto ceDto) throws Exception {
        CElectoral ce = new CElectoral();
        ce.setPoId(poid);
        ce.setDni(ceDto.getDni());
        ce.setFirstSurname(ceDto.getFirstSurname());
        ce.setSecondSurname(ceDto.getSecondSurname());
        ce.setNames(ceDto.getSecondSurname());
        ce.setNames(ceDto.getNames());
        ce.setPositionId(ceDto.getPositionId());
        ce.setValidStatusId(0);
        ce.setUsuarioCreacion(ceDto.getUsuarioCreacion());
        representativeMapper.saveCElectoralMember(ce);
    }

    @Override
    public void removeCElectoralMember(Integer id, String dni) throws Exception {
        representativeMapper.removeCElectoralMember(id, dni);
    }

    @Override
    public Integer countPadronRop() throws Exception {
        return representativeMapper.countPadronRop();
    }

    @Override
    public void saveArchive(POSaveArchiveInputDto inputDto) throws Exception {
        Archive archive = Archive.builder()
                .filename(inputDto.getFilename())
                .originalFilename(inputDto.getOriginalFilename())
                .size(inputDto.getSize())
                .format(inputDto.getFiletype())
                .path(inputDto.getPath())
                .guid("")
                .build();
        archive.setUsuarioCreacion(inputDto.getUsuarioCreacion());

        archiveMapper.saveArchive(archive);

        Integer giId = representativeMapper.getPoliticalOrganizationGeneralInfo(inputDto.getPoId()).getId();

        GeneralInfoArchive generalInfoArchive = GeneralInfoArchive.builder()
                .generalInfoId(giId)
                .archive(archive)
                .docId(inputDto.getDocType())
                .build();
        generalInfoArchive.setUsuarioCreacion(inputDto.getUsuarioCreacion());
        representativeMapper.saveArchive(generalInfoArchive);
    }

    @Override
    public List<POArchiveListOuputDto> listArchives(Integer id) throws Exception {
        List<POArchiveListOuputDto> ans = new ArrayList<>();
        representativeMapper.getArchivesByOp(id).forEach(a -> {
            POArchiveListOuputDto item = POArchiveListOuputDto.builder()
                    .id(a.getId())
                    .genInfoId(a.getGeneralInfoId())
                    .docType(a.getDocId())
                    .originalName(a.getArchive().getOriginalFilename())
                    .size(a.getArchive().getSize())
                    .build();
            ans.add(item);
        });

        return ans;
    }

    @Override
    public GeneralInfoArchive getArchivesByGI(Integer id) throws Exception {
        return representativeMapper.getArchivesByGI(id);
    }

    @Override
    public void removeFileByGI(Integer id) throws Exception {
        representativeMapper.deleteArchive(id);
    }
}
