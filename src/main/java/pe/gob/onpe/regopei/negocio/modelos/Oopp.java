package pe.gob.onpe.regopei.negocio.modelos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Oopp  extends ModeloBase {
	
	private Integer id;
	private DistritoElectoral distritoElectoral;
	private String idOopp;
	private DetCatalogoEstructura tipoOopp;
	private DetCatalogoEstructura estado;
	private String nombreOopp;
	
	
	private List<Oopp> lista;
	
	
	
	//Atributo para filtrar
	private Usuario usuario;
		
	


}
