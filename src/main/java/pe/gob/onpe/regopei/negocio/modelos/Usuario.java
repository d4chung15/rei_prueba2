package pe.gob.onpe.regopei.negocio.modelos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Usuario extends ModeloBase {

   private Integer 	idUsuario;
   private String 	usuario;
   private String 	abreviaturaPerfil;
   
   private List<Usuario> lista;

}
