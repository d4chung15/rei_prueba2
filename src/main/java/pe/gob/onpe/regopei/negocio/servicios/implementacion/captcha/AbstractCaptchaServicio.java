package pe.gob.onpe.regopei.negocio.servicios.implementacion.captcha;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestOperations;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.CaptchaServicio;
import pe.gob.onpe.regopei.transversal.excepciones.ReCaptchaInvalidException;
import pe.gob.onpe.regopei.transversal.properties.CaptchaProperties;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

@Log4j2
public class AbstractCaptchaServicio implements CaptchaServicio {


    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected CaptchaProperties captchaSettings;

    @Autowired
    protected ReCaptchaAttemptServicio reCaptchaAttemptService;

    @Autowired
    protected RestOperations restTemplate;

    protected static final Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

    protected static final String RECAPTCHA_URL_TEMPLATE = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";

    @Override
    public String getReCaptchaSite() {
        return captchaSettings.getSite();
    }

    @Override
    public String getReCaptchaSecret() {
        return captchaSettings.getSecret();
    }

    protected void securityCheck(final String response) {
        log.debug("Attempting to validate response {}", response);

        if (reCaptchaAttemptService.isBlocked(getClientIP())) {
            throw new ReCaptchaInvalidException("Client exceeded maximum number of failed attempts");
        }

        if (!responseSanityCheck(response)) {
            throw new ReCaptchaInvalidException("Response contains invalid characters");
        }
    }

    protected boolean responseSanityCheck(final String response) {
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }

    protected String getClientIP() {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

}
