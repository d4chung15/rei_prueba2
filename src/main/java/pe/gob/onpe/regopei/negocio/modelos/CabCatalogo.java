package pe.gob.onpe.regopei.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabCatalogo  extends ModeloBase{
	
	private Integer idCatalogo;
	private Integer idCatalogoPadre;
	private String  maestro;
	

}

