package pe.gob.onpe.regopei.negocio.modelos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DistritoElectoral extends ModeloBase {
	
	private Integer id;
	private Integer idPadre;
	private String nombre;
	
	private List<DistritoElectoral> lista;
	//filtro
	private Integer idCabRegistroModalidadEleccion;
	private Integer cantidadDelegados;
	private Integer circunscripcion;
	

}
