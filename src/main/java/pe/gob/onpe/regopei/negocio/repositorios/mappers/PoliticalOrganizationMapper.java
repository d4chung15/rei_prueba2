package pe.gob.onpe.regopei.negocio.repositorios.mappers;


import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.regopei.negocio.modelos.CElectoral;
import pe.gob.onpe.regopei.negocio.modelos.GeneralInfoArchive;
import pe.gob.onpe.regopei.negocio.modelos.PORepresentative;
import pe.gob.onpe.regopei.negocio.modelos.PoliticalOrganization;

import java.util.List;

@Mapper
public interface PoliticalOrganizationMapper {

    PoliticalOrganization getPoliticalOrganization(Integer id) throws Exception;
    PORepresentative getPoliticalOrganizationGeneralInfo(Integer id) throws Exception;

    void savePoliticalOrganizationRepresentative(PORepresentative poRepresentative) throws  Exception;

    List<CElectoral> listCElectoralByPOId(Integer poid) throws Exception;
    CElectoral getCElectoralMemberById(Integer id) throws  Exception;

    void saveCElectoralMember(CElectoral m) throws Exception;

    void removeCElectoralMember(Integer id, String dni) throws Exception;

    Integer countPadronRop() throws Exception;

    List<GeneralInfoArchive> getArchivesByOp(Integer id) throws Exception;
    void saveArchive(GeneralInfoArchive archive) throws Exception;
    void deleteArchive(Integer id) throws Exception;
    GeneralInfoArchive getArchivesByGI(Integer id) throws Exception;
}
