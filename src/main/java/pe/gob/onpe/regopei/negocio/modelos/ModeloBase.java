package pe.gob.onpe.regopei.negocio.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ModeloBase {
    private Integer activo;
    private String usuarioCreacion;
    private String usuarioModificacion;
    private Date fechaCreacion;
    private Date fechaModificacion;
    
    private Integer totalRegistroPorPagina;
    private Integer pagina;
    private Integer totalPaginas;
    private Integer totalRegistros;
  
    
    @JsonIgnore
    private Integer resultado;
    @JsonIgnore
    private String mensaje;
    @JsonIgnore
    private String mensajeInterno;

}
