package pe.gob.onpe.regopei.negocio.modelos;


import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CElectoral extends ModeloBase {
    private Integer id;
    private Integer poId;
    private String dni;
    private String firstSurname;
    private String secondSurname;
    private String names;
    private Integer positionId;
    private String position;
    private Integer validStatusId;
    private String validStatus;
}
