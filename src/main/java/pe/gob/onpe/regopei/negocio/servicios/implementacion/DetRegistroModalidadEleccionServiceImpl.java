package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.onpe.regopei.negocio.modelos.CabRegistroModalidadEleccion;
import pe.gob.onpe.regopei.negocio.modelos.DetRegistroModalidadEleccion;
import pe.gob.onpe.regopei.negocio.modelos.DistritoElectoral;
import pe.gob.onpe.regopei.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.regopei.negocio.repositorios.mappers.DetRegistroModalidadEleccionMapper;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.DetRegistroModalidadEleccionService;
import pe.gob.onpe.regopei.transversal.utilidades.Funciones;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ListarDistritoElectoralModalidadDelegadosOutputDto;

@Service
public class DetRegistroModalidadEleccionServiceImpl implements DetRegistroModalidadEleccionService {

	@Autowired
	private DetRegistroModalidadEleccionMapper mapper;
	
	@Override
	public void guardarActualizar(DetRegistroModalidadEleccion detRegistro) throws Exception {
		mapper.guardarActualizar(detRegistro);
		Funciones.validarOperacionConBaseDatos(detRegistro, DetRegistroModalidadEleccionServiceImpl.class, "guardarActualizar");
		
	}

	@Transactional
	@Override
	public void guardarActualizarLista(List<ListarDistritoElectoralModalidadDelegadosOutputDto> lista, CabRegistroModalidadEleccion cab)
			throws Exception {
		
		
		for(ListarDistritoElectoralModalidadDelegadosOutputDto dto : lista) {
			DetRegistroModalidadEleccion det = new DetRegistroModalidadEleccion();
			det.setRegistro(cab);
			
			MaeUbigeo ubigeo = new MaeUbigeo();
    		ubigeo.setDistritoElectoral(DistritoElectoral.builder().id(dto.getIdDistritoElectoral()).build());
    		if(dto.getIdDistritoElectoral() == 15) {
    			ubigeo.setCircunscripcion(2);
    		}else {
    			ubigeo.setCircunscripcion(1);
    		}
    	
    		det.setUbigeo(ubigeo);
    		det.setCantidadDelegados(dto.getCantidadDelegados());
    		det.setUsuarioCreacion(cab.getUsuarioCreacion());
    		mapper.guardarActualizar(det);
			
		}
		
	}

	@Override
	public void darBajaDetRegistroModalidadEleccion(DetRegistroModalidadEleccion detRegistro) throws Exception {
		mapper.darBajaPorCabRegistroModalidadEleccion(detRegistro);
		Funciones.validarOperacionConBaseDatos(detRegistro, DetRegistroModalidadEleccionServiceImpl.class, "darBajaDetRegistroModalidadEleccion");
		
	}

}
