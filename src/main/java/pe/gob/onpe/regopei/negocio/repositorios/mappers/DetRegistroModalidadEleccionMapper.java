package pe.gob.onpe.regopei.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.regopei.negocio.modelos.DetRegistroModalidadEleccion;

@Mapper
public interface DetRegistroModalidadEleccionMapper {

	
	public void guardarActualizar(DetRegistroModalidadEleccion registro) throws Exception;
	
	void darBajaPorCabRegistroModalidadEleccion(DetRegistroModalidadEleccion registro) throws Exception;
}
