package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.negocio.beans.Persona;

public interface PadronService {
	
	public void consultarPadron(Persona persona);

}
