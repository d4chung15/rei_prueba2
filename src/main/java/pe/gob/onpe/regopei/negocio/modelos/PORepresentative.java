package pe.gob.onpe.regopei.negocio.modelos;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PORepresentative extends ModeloBase {
    private Integer id;
    private Integer poId;
    private String dni;
    private String name;
    private String email;
    private String cellphone;
    private String phone;
    private String address;
}
