package pe.gob.onpe.regopei.negocio.modelos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CabRegistroModalidadEleccion  extends ModeloBase {
	
	private Integer id;
	private Oopp oopp;
	private DetCatalogoEstructura modalidadEleccion;
	private DetCatalogoEstructura modalidadCircunscripcionDelegados;
	private DetCatalogoEstructura tipoPresentacion;
	private DetCatalogoEstructura formaPresentacion;
	
	private List<CabRegistroModalidadEleccion> lista;
	
}
