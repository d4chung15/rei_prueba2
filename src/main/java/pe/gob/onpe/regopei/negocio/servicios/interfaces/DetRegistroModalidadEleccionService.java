package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import java.util.List;

import pe.gob.onpe.regopei.negocio.modelos.CabRegistroModalidadEleccion;
import pe.gob.onpe.regopei.negocio.modelos.DetRegistroModalidadEleccion;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ListarDistritoElectoralModalidadDelegadosOutputDto;

public interface DetRegistroModalidadEleccionService {
	
	 void guardarActualizar(DetRegistroModalidadEleccion detRegistro ) throws Exception;
	 
	 void guardarActualizarLista(List<ListarDistritoElectoralModalidadDelegadosOutputDto> lista, CabRegistroModalidadEleccion cab) throws Exception;

	 void darBajaDetRegistroModalidadEleccion(DetRegistroModalidadEleccion detRegistro) throws Exception;
}
