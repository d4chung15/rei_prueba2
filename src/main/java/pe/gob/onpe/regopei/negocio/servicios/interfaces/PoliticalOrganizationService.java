package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.negocio.modelos.GeneralInfoArchive;
import pe.gob.onpe.regopei.negocio.modelos.PORepresentative;
import pe.gob.onpe.regopei.negocio.modelos.PoliticalOrganization;
import pe.gob.onpe.regopei.vista.dtos.input.POSaveArchiveInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.CentralElectoralBodyMemberDto;
import pe.gob.onpe.regopei.vista.dtos.output.CentralElectoralBodyMembersOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.POArchiveListOuputDto;

import java.util.List;


public interface PoliticalOrganizationService {

    PoliticalOrganization getPoliticalOrganization(Integer id) throws Exception;
    PORepresentative getPoliticalOrganizationInfo(Integer id) throws Exception;

    void savePoliticalOrganizationRepresentative(PORepresentative poRepresentative) throws Exception;

    CentralElectoralBodyMembersOutputDto listCElectoralMembers(Integer id) throws Exception;
    void saveCElectoralBodyMember(Integer poid, CentralElectoralBodyMemberDto ce) throws Exception;

    void removeCElectoralMember(Integer id, String dni) throws Exception;

    Integer countPadronRop() throws Exception;

    void saveArchive(POSaveArchiveInputDto inputDto) throws Exception;
    List<POArchiveListOuputDto> listArchives(Integer id) throws Exception;

    GeneralInfoArchive getArchivesByGI(Integer id) throws Exception;

    void removeFileByGI(Integer id) throws Exception;
}
