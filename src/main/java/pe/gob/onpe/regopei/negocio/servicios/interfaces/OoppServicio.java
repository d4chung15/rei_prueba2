package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.negocio.modelos.Oopp;

public interface OoppServicio {
	
	void listarOoppPorUsuario(Oopp oopp) throws Exception;
	
	void obtenerOoppPorID(Oopp oopp) throws Exception;

}
