package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.negocio.modelos.DistritoElectoral;

public interface DistritoElectoralService {
	
	
	void obtenerDistritoElectoralModalidadDelegados(DistritoElectoral distritoElectoral) throws Exception;

}
