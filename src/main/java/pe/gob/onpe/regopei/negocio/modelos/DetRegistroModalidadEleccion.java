package pe.gob.onpe.regopei.negocio.modelos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetRegistroModalidadEleccion  extends ModeloBase {
	
	private Integer id;
	private CabRegistroModalidadEleccion registro;
	private MaeUbigeo ubigeo;
	private DetCatalogoEstructura modalidadCircunscripcionAfiliado;
	private Integer cantidadDelegados;

	private List<DetRegistroModalidadEleccion> lista;
	
	
	
	
	
		
	


}
