package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.negocio.modelos.Usuario;
import pe.gob.onpe.regopei.vista.dtos.input.LoginInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.ActualizarNuevaClaveInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.AsignarPersonaUsuarioGeneradoInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.CargarAccesosInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.BaseOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.CargarAccesoDatosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.LoginDatosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.RefrescarTokenOutputDto;

import java.util.HashMap;

public interface UsuarioServicio {

	LoginDatosOutputDto accederSistema(LoginInputDto input) throws Exception;

	CargarAccesoDatosOutputDto cargarAccesos(CargarAccesosInputDto input, String token) throws Exception;

	HashMap<String, Object> actualizarNuevaClave(ActualizarNuevaClaveInputDto input, String token) throws Exception;

	BaseOutputDto asignarPersonaAUsuarioGenerardo(AsignarPersonaUsuarioGeneradoInputDto input, String token)
			throws Exception;

	RefrescarTokenOutputDto refrescarToken(String token) throws Exception;

	Usuario obtenerUsuarioPorNumeroDocumento(String numeroDocumento);


   

}
