package pe.gob.onpe.regopei.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Aplicacion extends ModeloBase {
    private Integer idAplicacion;
    private String nombre;
    private String descripcion;
    private String codigo;
    private String url;

    private List<Aplicacion> aplicaciones = new ArrayList<>();
    private Aplicacion aplicacion;
//	private UnidadOrganica unidadOrganica;
}
