package pe.gob.onpe.regopei.negocio.modelos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PoliticalOrganization {
    private Integer id;
    private String poId;
    private String name;
    private DetCatalogoEstructura type;
    private DetCatalogoEstructura status;
}
