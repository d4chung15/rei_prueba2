package pe.gob.onpe.regopei.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.regopei.negocio.modelos.Archive;

@Mapper
public interface ArchiveMapper {
    Archive getArchive(Integer id) throws Exception;
    void saveArchive(Archive archive) throws Exception;
    void deleteArchive(Integer id) throws Exception;
}
