package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.regopei.negocio.modelos.CabCatalogo;
import pe.gob.onpe.regopei.negocio.repositorios.mappers.CatalogMapper;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.CatalogService;
import pe.gob.onpe.regopei.vista.dtos.output.CatalogDetailOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.CatalogOutputDto;

import java.util.ArrayList;
import java.util.List;


@Service
public class CatalogServiceImpl implements CatalogService {

    @Autowired
    private CatalogMapper mapper;

    @Override
    public CatalogOutputDto findCatalogByMaestro(String maestro) throws Exception {
        CatalogOutputDto ans = new CatalogOutputDto();

        CabCatalogo cat = mapper.getCatalogByMaestro(maestro);
        ans.setId(cat.getIdCatalogo());
        ans.setName(cat.getMaestro());
        ans.setParent(cat.getIdCatalogoPadre());

        List<CatalogDetailOutputDto> options = new ArrayList<>();
        mapper.getCatalogDetalleByMaestro(maestro)
                .forEach(d -> options.add(
                        new CatalogDetailOutputDto(d.getIdDetCatalogoEstructura(),
                        d.getNombre(),
                        d.getCodigo(),
                        d.getOrden()))
                );
        ans.setOptions(options);

        return ans;
    }
}
