package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.negocio.modelos.CabCatalogo;
import pe.gob.onpe.regopei.negocio.modelos.DetCatalogoEstructura;
import pe.gob.onpe.regopei.vista.dtos.output.CatalogOutputDto;

import java.util.List;

public interface CatalogService {
    public CatalogOutputDto findCatalogByMaestro(String maestro) throws Exception;
}
