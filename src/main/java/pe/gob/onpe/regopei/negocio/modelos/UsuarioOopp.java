package pe.gob.onpe.regopei.negocio.modelos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsuarioOopp extends ModeloBase {

   private Integer 	id;
   private Usuario usuario;
   private Oopp oopp;
   private List<UsuarioOopp> lista;

}
