package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pe.gob.onpe.regopei.negocio.modelos.Usuario;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.UsuarioServicio;
import pe.gob.onpe.regopei.transversal.properties.SasaProperties;
import pe.gob.onpe.regopei.transversal.utilidades.Funciones;
import pe.gob.onpe.regopei.vista.dtos.input.LoginInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.ActualizarNuevaClaveInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.AsignarPersonaUsuarioGeneradoInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.CargarAccesosInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.BaseOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.CargarAccesoDatosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.LoginDatosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.RefrescarTokenOutputDto;

import java.util.HashMap;


@Service
public class UsuarioServicioImpl implements UsuarioServicio {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private SasaProperties sasaProperties;
	
	@Override
	public Usuario obtenerUsuarioPorNumeroDocumento(String numeroDocumento) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public LoginDatosOutputDto accederSistema(LoginInputDto input) throws Exception {
		LoginDatosOutputDto datos = null;
		
				input.setCodigo(sasaProperties.getAplicacion());
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("Content-Type", "application/json");
				HttpEntity<LoginInputDto> requestEntity = new HttpEntity<LoginInputDto>(input,
						headers);
				
				try {
						ResponseEntity<LoginDatosOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/loginsc",
						HttpMethod.POST, requestEntity, LoginDatosOutputDto.class);
						datos = responseLogin.getBody();
						
				}catch(Exception e) {
					e.printStackTrace();
					Usuario usuario = new Usuario();
					usuario.setResultado(-2);
					usuario.setMensaje("El nombre de usuario y/o contraseña son incorrectos.");
					Funciones.validarOperacionConBaseDatos(usuario, UsuarioServicioImpl.class, "accederSistema");
					
				}
				
			return datos;
		
	}
		
	@Override
	public CargarAccesoDatosOutputDto cargarAccesos(CargarAccesosInputDto input,String token) throws Exception {
	try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<CargarAccesosInputDto> requestEntity = new HttpEntity<CargarAccesosInputDto>(input,headers);

			ResponseEntity<CargarAccesoDatosOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/cargar-accesos",
					HttpMethod.POST, requestEntity, CargarAccesoDatosOutputDto.class);

			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

	@Override
	public HashMap<String, Object> actualizarNuevaClave(ActualizarNuevaClaveInputDto input, String token)
			throws Exception {
	try {
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<ActualizarNuevaClaveInputDto> requestEntity = new HttpEntity<ActualizarNuevaClaveInputDto>(input,headers);

			
			ResponseEntity<HashMap> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/actualizar-nueva-clave",
					HttpMethod.POST, requestEntity, HashMap.class);
	
					
			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

	@Override
	public BaseOutputDto asignarPersonaAUsuarioGenerardo(AsignarPersonaUsuarioGeneradoInputDto input, String token)
			throws Exception {
		try {
			
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<AsignarPersonaUsuarioGeneradoInputDto> requestEntity = new HttpEntity<AsignarPersonaUsuarioGeneradoInputDto>(input,headers);

			
			ResponseEntity<BaseOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/asignar-persona-usuario-autogenerado",
					HttpMethod.POST, requestEntity, BaseOutputDto.class);

			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

	@Override
	public RefrescarTokenOutputDto refrescarToken(String token) throws Exception {
	try {
			
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<RefrescarTokenOutputDto> requestEntity = new HttpEntity<RefrescarTokenOutputDto>(headers);

			
			ResponseEntity<RefrescarTokenOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/refreshtoken",
					HttpMethod.GET, requestEntity, RefrescarTokenOutputDto.class);

							
			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

}
