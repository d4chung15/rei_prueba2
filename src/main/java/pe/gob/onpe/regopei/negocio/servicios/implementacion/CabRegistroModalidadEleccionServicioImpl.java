package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.regopei.negocio.modelos.CabRegistroModalidadEleccion;
import pe.gob.onpe.regopei.negocio.modelos.DetRegistroModalidadEleccion;
import pe.gob.onpe.regopei.negocio.modelos.DistritoElectoral;
import pe.gob.onpe.regopei.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.regopei.negocio.repositorios.mappers.CabRegistroModalidadEleccionMapper;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.CabRegistroModalidadEleccionServicio;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.DetRegistroModalidadEleccionService;
import pe.gob.onpe.regopei.transversal.utilidades.Funciones;
import pe.gob.onpe.regopei.vista.dtos.input.oopp.RegistrarModalidadEleccionInputDto;

@Service
public class CabRegistroModalidadEleccionServicioImpl implements CabRegistroModalidadEleccionServicio {

	
	@Autowired
	private CabRegistroModalidadEleccionMapper mapper;
	
	@Autowired
	private DetRegistroModalidadEleccionService detalleServicio;
	
	@Override
	public void obtenerPorOoop(CabRegistroModalidadEleccion param) throws Exception {
		mapper.obtenerPorOopp(param);
		Funciones.validarOperacionConBaseDatos(param, CabRegistroModalidadEleccionServicioImpl.class, "obtenerPorOopp");
		
	}

	@Override
	public void guardarActualizar(CabRegistroModalidadEleccion param, RegistrarModalidadEleccionInputDto input) throws Exception {
		
		mapper.guardarActualizar(param);
		
		if(param.getResultado() == 1) {
			
			DetRegistroModalidadEleccion darBaja = new DetRegistroModalidadEleccion();
			darBaja.setRegistro(param);
			darBaja.setUsuarioCreacion(param.getUsuarioCreacion());
			detalleServicio.darBajaDetRegistroModalidadEleccion(darBaja);
			
			if(param.getModalidadEleccion() != null  && param.getModalidadEleccion().getCodigo() != 1) {
	    		if(input.getCantidadDelegados() != null) {
	        		DetRegistroModalidadEleccion det = new DetRegistroModalidadEleccion();
	        		det.setRegistro(param);
	        		
	        		MaeUbigeo ubigeo = new MaeUbigeo();
	        		ubigeo.setDistritoElectoral(DistritoElectoral.builder().id(input.getIdDistritoElectoral()).build());
	        		ubigeo.setCircunscripcion(input.getCodigoModalidadCircunscripcionD()==0?0:1);
	        		det.setUbigeo(ubigeo);
	        		det.setCantidadDelegados(input.getCantidadDelegados());
	        		det.setUsuarioCreacion(param.getUsuarioCreacion());
	        		detalleServicio.guardarActualizar(det);
	        	} else if(!input.getLista().isEmpty()) {
	        		detalleServicio.guardarActualizarLista(input.getLista(), param);
	        	}
	        	
	    	}	
		}
		
		
		Funciones.validarOperacionConBaseDatos(param, CabRegistroModalidadEleccionServicioImpl.class, "guardarActualizar");
		
	}

}
