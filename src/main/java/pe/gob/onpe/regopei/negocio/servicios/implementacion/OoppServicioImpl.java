package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.regopei.negocio.modelos.Oopp;
import pe.gob.onpe.regopei.negocio.repositorios.mappers.OoppMapper;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.OoppServicio;
import pe.gob.onpe.regopei.transversal.utilidades.Funciones;

@Service
public class OoppServicioImpl implements OoppServicio {
	
	@Autowired
	private OoppMapper ooppMapper;

	@Override
	public void listarOoppPorUsuario(Oopp oopp) throws Exception {
		ooppMapper.listarOoppPorUsuario(oopp);
		Funciones.validarOperacionConBaseDatos(oopp, OoppServicioImpl.class, "listarOoppPorUsuario");
	}

	@Override
	public void obtenerOoppPorID(Oopp oopp) throws Exception {
		ooppMapper.obtenerOoppPorId(oopp);
		Funciones.validarOperacionConBaseDatos(oopp, OoppServicioImpl.class, "obtenerOoppPorID");
		
	}

}
