package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.negocio.modelos.CabRegistroModalidadEleccion;
import pe.gob.onpe.regopei.vista.dtos.input.oopp.RegistrarModalidadEleccionInputDto;

public interface CabRegistroModalidadEleccionServicio {
	
	
	void obtenerPorOoop(CabRegistroModalidadEleccion param ) throws Exception;

	void guardarActualizar(CabRegistroModalidadEleccion param, RegistrarModalidadEleccionInputDto input) throws Exception;
	
}
