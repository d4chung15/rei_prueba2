package pe.gob.onpe.regopei.negocio.servicios.interfaces;

import pe.gob.onpe.regopei.transversal.excepciones.ReCaptchaInvalidException;

public interface CaptchaServicio {
    default void processResponse(final String response) throws ReCaptchaInvalidException {
    }

    default void processResponse(final String response, String action) throws ReCaptchaInvalidException {
    }

    String getReCaptchaSite();

    String getReCaptchaSecret();
}
