package pe.gob.onpe.regopei.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.regopei.negocio.modelos.DistritoElectoral;


@Mapper
public interface DistritoElectoralMapper {
	
   void obtenerDistritoElectoralModalidadDelegados(DistritoElectoral distritoElectoral) throws Exception;
   
   
   
}
