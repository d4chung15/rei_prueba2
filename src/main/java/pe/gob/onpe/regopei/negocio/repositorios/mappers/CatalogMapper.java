package pe.gob.onpe.regopei.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.regopei.negocio.modelos.CabCatalogo;
import pe.gob.onpe.regopei.negocio.modelos.DetCatalogoEstructura;

import java.util.List;

@Mapper
public interface CatalogMapper {
    public CabCatalogo getCatalogByMaestro(String maestro) throws Exception;
    public List<DetCatalogoEstructura> getCatalogDetalleByMaestro(String maestro) throws  Exception;
}
