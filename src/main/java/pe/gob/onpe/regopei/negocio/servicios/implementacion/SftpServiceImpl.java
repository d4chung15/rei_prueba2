package pe.gob.onpe.regopei.negocio.servicios.implementacion;

import com.jcraft.jsch.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.SftpService;
import pe.gob.onpe.regopei.transversal.properties.SftpProperties;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@Service
public class SftpServiceImpl implements SftpService {

    @Autowired
    private SftpProperties properties;

    private Session sftpSessionFactory() throws JSchException {
        JSch jSch = new JSch();

        Session session = jSch.getSession(properties.getFtpUsuario(), properties.getFtpIpServer(), Integer.parseInt(properties.getFtpPort()));
        session.setPassword(properties.getFtpPassword());

        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();

        return session;
    }

    @Override
    public boolean uploadFile(String filePath, InputStream fileInputStream) {
        Session session = null;
        try {
            session = sftpSessionFactory();
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();

            Path p = Paths.get(filePath);
            List<String> paths = new ArrayList<>();
            while (p.getParent() != null) {
                paths.add(p.getParent().toString());
                p = p.getParent();
            }

            Collections.reverse(paths);
            for (String path: paths) {
                try {
                    channelSftp.mkdir(path);
                } catch (Exception ignored) {
                }
            }

            channelSftp.put(fileInputStream, filePath);
            channelSftp.disconnect();
        } catch (JSchException | SftpException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (session != null) {
                session.disconnect();
            }
        }
        return true;
    }

    @Override
    public boolean removeFile(String path) {
        Session session = null;
        try {
            session = sftpSessionFactory();
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();

            if (channelSftp.ls(path).size() == 0) {
                return false;
            }
            channelSftp.rm(path);
            channelSftp.disconnect();
        } catch (JSchException | SftpException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (session != null) {
                session.disconnect();
            }
        }
        return true;
    }

    @Override
    public boolean downloadFile(String path, OutputStream outputStream) {
        Session session = null;
        try {
            session = sftpSessionFactory();
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();

            channelSftp.get(path, outputStream);
            channelSftp.disconnect();
        } catch (JSchException | SftpException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (session != null) {
                session.disconnect();
            }
        }
        return true;
    }
}
