package pe.gob.onpe.regopei.transversal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pe.gob.onpe.regopei.seguridad.filtros.JWTAuthenticationFilter;
import pe.gob.onpe.regopei.seguridad.jwt.JWTAuthenticationEntryPoint;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    // Esto es para el caso que usemos jsp
    String[] resources = new String[]{"/include/**", "/css/**", "/icons/**", "/img/**", "/js/**", "/layer/**"};
    final private JWTAuthenticationEntryPoint unauthorizedHandler;
    // final private UsuarioServicio usuarioServicio;
    final private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurityConfig(JWTAuthenticationEntryPoint unauthorizedHandler,
                             BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.unauthorizedHandler = unauthorizedHandler;
        // this.usuarioServicio = usuarioServicio;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    // overloaded only for this and @bean so that we can auto-wire in our
    // controllers
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public JWTAuthenticationFilter jwtAuthenticationFilter() {
        return new JWTAuthenticationFilter();
    }

    /**
     * Securing the urls and allowing role-based access to these urls.
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(ConstanteApi.URL_WEB_LIBRES);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        /*
         * http .headers() .addHeaderWriter(new
         * XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.
         * SAMEORIGIN));
         */

        //System.out.println("INGRESE OTRO SECURITY 1.");
        http
            .cors()
            .and().csrf().disable()
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler) // add exception handler like 401
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // REST Apis are
            .and()
            .antMatcher("/**")
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "**").permitAll()//allow CORS option calls
            .antMatchers(ConstanteApi.APIS_LIBRES).permitAll()
            .antMatchers("/", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html", "/**/*.css", "/**/*.js").permitAll();

        http.headers().frameOptions().sameOrigin().httpStrictTransportSecurity().disable();
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        ///System.out.println("INGRESE OTRO SECURITY 2.");
    }
}
