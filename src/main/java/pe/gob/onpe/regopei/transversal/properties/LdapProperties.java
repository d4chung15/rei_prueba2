package pe.gob.onpe.regopei.transversal.properties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@NoArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "ldap")
public class LdapProperties {
    private String url;
    private String base;
    private String principal;
    private String password;
    private Boolean ssl;
    private String domain;
}
