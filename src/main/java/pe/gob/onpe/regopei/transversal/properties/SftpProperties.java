package pe.gob.onpe.regopei.transversal.properties;

import java.util.Properties;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class SftpProperties {
  
    @Resource(mappedName = "resource/rei_properties")
	private Properties properties;
	private String ftpIpServer;
	private String ftpUsuario;
	private String ftpPassword;
	private String ftpPort;
	private String ftpDirhome;

	public String getFtpIpServer() {
		return this.getProperty("FTP_IPSERVER");
	}

	public String getFtpUsuario() {
		return this.getProperty("FTP_USUARIO");
	}

	public String getFtpPassword() {
		return this.getProperty("FTP_PASSWORD");
	}

	public String getFtpPort() {
		return this.getProperty("FTP_PORT");
	}

	public String getFtpDirhome() {
		return this.getProperty("FTP_DIRHOME");
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
    
    
}
