package pe.gob.onpe.regopei.transversal.constantes;

public class ConstanteApi {
    public static final String VERSION_API = "/v1";
    public static final String[] APIS_LIBRES = new String[]{"/csrf",
            ConstanteApi.VERSION_API + "/api/usuario/acceder-sistema",
            ConstanteApi.VERSION_API + "/api/usuario/registrationCaptchaV3",
            ConstanteApi.VERSION_API + "/api/usuario/test",
            ConstanteApi.VERSION_API + "/api/usuario/refreshtoken",
            ConstanteApi.VERSION_API + "/api/usuario/login",
            ConstanteApi.VERSION_API + "/api/usuario",
            ConstanteApi.VERSION_API + "/api/usuario/login",
            ConstanteApi.VERSION_API + "/api/proveedor/obtener-info-sunat",
            ConstanteApi.VERSION_API + "/api/proveedor/obtener-info-sunat-por-ruc-captcha",
            ConstanteApi.VERSION_API + "/api/mail/prueba",
            ConstanteApi.VERSION_API + "/api/usuario/generar-sesion-activa",
            ConstanteApi.VERSION_API + "/api/usuario/validar-sesion"

            // , ConstanteApi.VERSION_API + "/api/proveedor/generar-reporte-proveedor"
            , ConstanteApi.VERSION_API + "/", "/"};

    public static final String[] URL_WEB_LIBRES = new String[]{"/v2/api-docs", "/webjars", "/swagger-resources",
            "/configuration/ui", "/swagger-resources/**", "/configuration/security", "/swagger-ui.html",
            "/webjars/**"};


    public static final Integer TIPO_DOCUMENTO_DNI = 1;
}
