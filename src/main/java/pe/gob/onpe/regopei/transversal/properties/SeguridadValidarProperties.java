package pe.gob.onpe.regopei.transversal.properties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class SeguridadValidarProperties {

	@Resource(mappedName = "resource/rei_properties")
	private Properties properties;

	private Boolean token;

	public Boolean getToken() {
		return Boolean.parseBoolean(this.getProperty("seguridad.validar.token"));
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}