package pe.gob.onpe.regopei.transversal.properties;

import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class SasaProperties {

	@Resource(mappedName = "resource/rei_properties")
	private Properties properties;

	private String url;
	private String aplicacion;

	public String getUrl() {
		return this.getProperty("sasa.url");
	}

	public String getAplicacion() {
		return this.getProperty("sasa.aplicacion");
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}

