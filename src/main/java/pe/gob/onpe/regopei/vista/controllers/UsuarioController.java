package pe.gob.onpe.regopei.vista.controllers;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pe.gob.onpe.regopei.negocio.servicios.implementacion.captcha.CaptchaServicioV3;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.CaptchaServicio;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.UsuarioServicio;
import pe.gob.onpe.regopei.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteJwt;
import pe.gob.onpe.regopei.transversal.properties.ValidarCustomProperties;
import pe.gob.onpe.regopei.transversal.utilidades.Funciones;
import pe.gob.onpe.regopei.vista.dtos.input.LoginInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.ActualizarNuevaClaveInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.AsignarPersonaUsuarioGeneradoInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.seguridad.CargarAccesosInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.CargarAccesoDatosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.seguridad.LoginDatosOutputDto;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Validated
@RequestMapping(ConstanteApi.VERSION_API + "/api/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioServicio usuarioServicio;

	@Autowired
	private JWTTokenProvider jwtTokenProvider;

	@Autowired
	private ValidarCustomProperties validarCustomProperties;
	
	@Autowired
	private CaptchaServicio captchaServiceV3;
	
	public UsuarioController() {

	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody @Valid final LoginInputDto paramInputDto) throws Exception {
		// Implement
	
			//LoginOutputDto outputDto = usuarioServicio.accederSistema(paramInputDto);
		// Implement
		String recaptcha = paramInputDto.getRecaptcha();
		if(validarCustomProperties.getServicioCaptcha()) {
					captchaServiceV3.processResponse(recaptcha, CaptchaServicioV3.IMPORTANT_ACTION);
		}
			LoginDatosOutputDto datos = usuarioServicio.accederSistema(paramInputDto);
		
			HashMap<String, Object> resp = new HashMap<>();
			resp.put("resultado", 1);
			resp.put("mensaje", "La operación se ejecutó correctamente.");
			resp.put("datos", datos.getDatos());
			return new ResponseEntity<>(resp, HttpStatus.OK);
		
	}
	
	@PostMapping("/cargar-accesos")
	public ResponseEntity<?> cargarAccesos(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token ,@RequestBody @Valid final CargarAccesosInputDto paramInputDto) throws Exception {
		
		CargarAccesoDatosOutputDto datos =  usuarioServicio.cargarAccesos(paramInputDto,token);
		
		HashMap<String,Object> resp = new HashMap<>();
		resp.put("resultado", 1);
		resp.put("mensaje", "La operación se ejecutó correctamente.");
		resp.put("datos", datos.getDatos());
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@PostMapping("/actualizar-nueva-clave")
	public ResponseEntity<?> actualizarNuevaClave(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid final ActualizarNuevaClaveInputDto paramInputDto) throws Exception {
		
		HashMap<String, Object> resp = usuarioServicio.actualizarNuevaClave(paramInputDto,token);
		resp.put("resultado", 1);
		resp.put("mensaje","La operación se ejecutó correctamente.");
	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@GetMapping("/refreshtoken")
	public ResponseEntity<?> refreshtoken(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token)
			throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();

		token = jwtTokenProvider.obtenerSoloToken(token);
		Claims claims = Jwts.parser().setSigningKey(ConstanteJwt.SECRET).parseClaimsJws(token).getBody();

		Map<String, Object> expectedMap = Funciones.getMapFromIoJsonwebtokenClaims((DefaultClaims) claims);

		String tokenRefresh = jwtTokenProvider.generarRefreshToken(expectedMap,
				expectedMap.get("numeroDocumento").toString());

		HashMap<String, Object> response = new HashMap<String, Object>();

		response.put("resultado", 1);
		response.put("mensaje", "todo correcto");
		response.put("token", tokenRefresh);

		return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
	}
	
	@PostMapping("/asignar-persona-usuario-autogenerado")
	public ResponseEntity<?> asignarPersonaUsuarioAutogenerado(@RequestHeader(value =
	        HttpHeaders.AUTHORIZATION) String token,@RequestBody @Valid AsignarPersonaUsuarioGeneradoInputDto usuarioInputDto)
			throws Exception {
		
		return new ResponseEntity<>(usuarioServicio.asignarPersonaAUsuarioGenerardo(usuarioInputDto, token), HttpStatus.OK);

	}

}
