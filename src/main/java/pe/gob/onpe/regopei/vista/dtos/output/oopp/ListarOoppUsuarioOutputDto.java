package pe.gob.onpe.regopei.vista.dtos.output.oopp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarOoppUsuarioOutputDto {
	
	private Integer id;
	private String nombreTipo;
	private String nombreOopp;
	private String region;

}
