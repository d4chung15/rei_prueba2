package pe.gob.onpe.regopei.vista.dtos.input.oopp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarDistritoElectoralModalidadDelegadosInputDto {
	
	private Integer idDistrito;
	private Integer idDistritoPadre;
	private Integer idCabRegistroModalidadEleccion;
	private Integer circunscripcion;

}
