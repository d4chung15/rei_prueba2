package pe.gob.onpe.regopei.vista.dtos.output.oopp;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.regopei.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseRegistroModalidadEleccionOutputDto extends BaseOutputDto{
	
	private DatosRegistrarModalidadEleccionOutputDto datos;

}
