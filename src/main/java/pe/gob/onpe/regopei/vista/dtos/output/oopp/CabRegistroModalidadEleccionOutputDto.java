package pe.gob.onpe.regopei.vista.dtos.output.oopp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabRegistroModalidadEleccionOutputDto {
	
	private Integer id;
	private Integer idOopp;
	private Integer codigoModalidad;
	private Integer codigoModalidadCircunscripcionD;
	private Integer tipoPresentacion;
	private Integer formaPresentacion;

}
