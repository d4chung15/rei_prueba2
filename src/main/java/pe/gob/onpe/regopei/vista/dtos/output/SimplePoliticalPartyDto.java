package pe.gob.onpe.regopei.vista.dtos.output;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SimplePoliticalPartyDto {

    private Integer id;
    private String name;
}
