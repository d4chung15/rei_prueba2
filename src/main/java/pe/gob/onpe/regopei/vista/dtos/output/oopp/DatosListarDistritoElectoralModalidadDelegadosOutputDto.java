package pe.gob.onpe.regopei.vista.dtos.output.oopp;


import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosListarDistritoElectoralModalidadDelegadosOutputDto {
	
	private List<ListarDistritoElectoralModalidadDelegadosOutputDto> lista;
	

}
