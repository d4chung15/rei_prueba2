package pe.gob.onpe.regopei.vista.dtos.wrapper;



import pe.gob.onpe.regopei.negocio.modelos.CabRegistroModalidadEleccion;
import pe.gob.onpe.regopei.negocio.modelos.DetCatalogoEstructura;
import pe.gob.onpe.regopei.negocio.modelos.Oopp;
import pe.gob.onpe.regopei.vista.dtos.input.oopp.RegistrarModalidadEleccionInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.CabRegistroModalidadEleccionOutputDto;


public class CabModalidadEleccionWrapper {
	
	public static CabRegistroModalidadEleccionOutputDto CabRegistroModalidadEleccionOutputDtoToModelWrapper(CabRegistroModalidadEleccion origen) {
		
		if(origen == null) {
			return null;
		}
		
		
		
		CabRegistroModalidadEleccionOutputDto dto = new CabRegistroModalidadEleccionOutputDto();
		dto.setId(origen.getId());
		dto.setIdOopp(origen.getOopp() != null ? origen.getOopp().getId():null);
		dto.setCodigoModalidad(origen.getModalidadEleccion() != null ? origen.getModalidadEleccion().getCodigo():null);
		dto.setCodigoModalidadCircunscripcionD(origen.getModalidadCircunscripcionDelegados() != null ? origen.getModalidadCircunscripcionDelegados().getCodigo():null);
		dto.setTipoPresentacion(origen.getTipoPresentacion() != null ? origen.getTipoPresentacion().getCodigo() :null);
		dto.setFormaPresentacion(origen.getFormaPresentacion() != null ? origen.getFormaPresentacion().getCodigo() :null);
		return dto;
	}	
	
	
public static CabRegistroModalidadEleccion CabRegistroModalidadEleccionToModelWrapper(RegistrarModalidadEleccionInputDto origen) {
		
		if(origen == null) {
			return null;
		}
		
		CabRegistroModalidadEleccion dto = new CabRegistroModalidadEleccion();
		
		Oopp oopp = new Oopp();
		oopp.setId(origen.getIdOopp());
		dto.setOopp(oopp);
		
		if(origen.getCodigoModalidad() != null) {
			DetCatalogoEstructura modalidad = new DetCatalogoEstructura();
			modalidad.setCodigo(origen.getCodigoModalidad());
			dto.setModalidadEleccion(modalidad);
		}
		
		if(origen.getCodigoTipoPresentacion() != null) {
			DetCatalogoEstructura tipoPresentacion = new DetCatalogoEstructura();
			tipoPresentacion.setCodigo(origen.getCodigoTipoPresentacion());
			dto.setTipoPresentacion(tipoPresentacion);
		}
		
		if(origen.getCodigoModalidadCircunscripcionD() != null) {
			DetCatalogoEstructura modalidadC = new DetCatalogoEstructura();
			modalidadC.setCodigo(origen.getCodigoModalidadCircunscripcionD());
			dto.setModalidadCircunscripcionDelegados(modalidadC);
		}
		
		if(origen.getCodigoFormaPresentacion() != null) {
			DetCatalogoEstructura formaPresentacion = new DetCatalogoEstructura();
			formaPresentacion.setCodigo(origen.getCodigoFormaPresentacion());
			dto.setFormaPresentacion(formaPresentacion);
		}
		
		return dto;
	}

}
