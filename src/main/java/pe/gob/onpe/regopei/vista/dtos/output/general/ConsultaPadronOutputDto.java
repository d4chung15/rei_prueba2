package pe.gob.onpe.regopei.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaPadronOutputDto {
	
	
	private String numele;
	private String digver;
	private String nummesa;
	private String ubigeo;
	private String appat;
	private String apmat;
	private String nombres;
	private String fecnac;
	private String sexo;
	private String gradins;
	private String restric;
	private String tipodoc;
	private String discap;
	private String foto;
	
	private String mensaje;

}
