package pe.gob.onpe.regopei.vista.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.onpe.regopei.negocio.modelos.DistritoElectoral;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.DistritoElectoralService;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.vista.dtos.input.oopp.ListarDistritoElectoralModalidadDelegadosInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.DatosListarDistritoElectoralModalidadDelegadosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ListarDistritoElectoralModalidadDelegadosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ResponseListarDistritoElectoralModalidadDelegadosOutputDto;
import pe.gob.onpe.regopei.vista.dtos.wrapper.DistritoElectoralWrapper;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/distrito-electoral")
@Validated
public class DistritoElectoralController {
	
	@Autowired
	private DistritoElectoralService distritoElectoralService;
    


    @PostMapping("/obtener-distrito-electoral")
    public ResponseEntity<?> obtenerDistritoElectoral( @RequestBody ListarDistritoElectoralModalidadDelegadosInputDto input ) throws Exception {
    	
    	
    	DistritoElectoral disitritoElectoral = new DistritoElectoral();
    	disitritoElectoral.setId(input.getIdDistrito());
    	disitritoElectoral.setIdPadre(input.getIdDistritoPadre());
    	disitritoElectoral.setIdCabRegistroModalidadEleccion(input.getIdCabRegistroModalidadEleccion());
    	disitritoElectoral.setCircunscripcion(input.getCircunscripcion());
    	
    	distritoElectoralService.obtenerDistritoElectoralModalidadDelegados(disitritoElectoral);
    	
    	
    	List<ListarDistritoElectoralModalidadDelegadosOutputDto> out = DistritoElectoralWrapper.ListarDistritoElectoralModalidadDelegadosOutputDtoToModelWrapper(disitritoElectoral.getLista().isEmpty()?null:disitritoElectoral.getLista());
    	
    	
       

    	
    	DatosListarDistritoElectoralModalidadDelegadosOutputDto datos = new DatosListarDistritoElectoralModalidadDelegadosOutputDto();
    	datos.setLista(out);
    	
    	ResponseListarDistritoElectoralModalidadDelegadosOutputDto response = new ResponseListarDistritoElectoralModalidadDelegadosOutputDto();
        response.setResultado(disitritoElectoral.getResultado());
        response.setMensaje( disitritoElectoral.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    
    
}
