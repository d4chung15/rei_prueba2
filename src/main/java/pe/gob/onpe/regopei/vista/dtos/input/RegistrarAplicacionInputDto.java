package pe.gob.onpe.regopei.vista.dtos.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrarAplicacionInputDto {
    @NotBlank(message = "El nombre no puede ser nulo ni estar en blanco")
    @NotNull(message = "El nombre no puede ser nulo")
    private String nombre;

    private String descripcion;

    @NotBlank(message = "El código no puede ser nulo ni estar en blanco")
    @NotNull(message = "El código no puede ser nulo")
    private String codigo;

    private String url;
    @NotNull(message = "El idUnidadOrganica no puede ser nulo")
    private Integer idUnidadOrganica;
}
