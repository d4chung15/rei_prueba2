package pe.gob.onpe.regopei.vista.dtos.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CatalogDetailOutputDto {
    private Integer catalogId;
    private String name;
    private Object code;
    private Integer order;
}
