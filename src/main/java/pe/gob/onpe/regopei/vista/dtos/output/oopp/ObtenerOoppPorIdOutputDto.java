package pe.gob.onpe.regopei.vista.dtos.output.oopp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerOoppPorIdOutputDto {
	
	private Integer id;
	private Integer tipo;
	private String nombreTipo;
	private String nombreOopp;
	private Integer idDistritoElectoral;
	private String region;

}
