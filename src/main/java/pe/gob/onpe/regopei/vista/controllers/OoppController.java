package pe.gob.onpe.regopei.vista.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import pe.gob.onpe.regopei.negocio.modelos.Oopp;
import pe.gob.onpe.regopei.negocio.modelos.Usuario;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.OoppServicio;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.vista.dtos.input.oopp.ListarOoppUsuarioInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.DatosListarOoppUsuarioOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.DatosObtenerOoppPorIdOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ListarOoppUsuarioOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ObtenerOoppPorIdOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ResponseListarOoppUsuarioOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ResponseObtenerOoppPorIdOutputDto;
import pe.gob.onpe.regopei.vista.dtos.wrapper.OoppWrapper;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/oopp")
@Validated
public class OoppController {
    
    @Autowired
    private OoppServicio oopServicio;

    @ApiOperation(value = "Obtener nombre de persona por dni", notes = "Obtener nombre de persona por dni")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
   
    @PostMapping("/listar-oopp-usuario")
    public ResponseEntity<?> listarOoppPorUsuario(@RequestBody ListarOoppUsuarioInputDto param) throws Exception {

    	Oopp oopp = new Oopp();
    	
    	Usuario usuario = Usuario
    			.builder()
    			.abreviaturaPerfil(param.getPerfil())
    			.usuario(param.getUsuario())
    			
    			.build();
    	oopp.setNombreOopp(param.getNombreOopp());
    	oopp.setUsuario(usuario);
    	oopp.setPagina(param.getPagina());
    	oopp.setTotalRegistroPorPagina(param.getTotalRegistroPorPagina());
    	
    	oopServicio.listarOoppPorUsuario(oopp);
    	
    	List<ListarOoppUsuarioOutputDto> lista = OoppWrapper.ListarOoppUsuarioOutputDtoToModelWrapper(oopp.getLista());
    	
        DatosListarOoppUsuarioOutputDto  datos = new DatosListarOoppUsuarioOutputDto();
        datos.setOopps(lista);
        datos.setTotalRegistros(!oopp.getLista().isEmpty() ?oopp.getLista().get(0).getTotalRegistros():0);

        ResponseListarOoppUsuarioOutputDto response = new ResponseListarOoppUsuarioOutputDto();
        response.setResultado(oopp.getResultado());
        response.setMensaje( oopp.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @GetMapping("/obtener-oopp/{idOopp}")
    public ResponseEntity<?> obtenerOopp(@PathVariable ("idOopp") Integer idOopp ) throws Exception {

    	Oopp oopp = new Oopp();
    	oopp.setId(idOopp);
    	
    	oopServicio.obtenerOoppPorID(oopp);
    	
    	ObtenerOoppPorIdOutputDto out = OoppWrapper.ObtenerOoppPorIdOutputDtoToModelWrapper(oopp.getLista().isEmpty()?null:oopp.getLista().get(0));
    	
    	
        DatosObtenerOoppPorIdOutputDto  datos = new DatosObtenerOoppPorIdOutputDto();
        datos.setOopp(out);
      

        ResponseObtenerOoppPorIdOutputDto response = new ResponseObtenerOoppPorIdOutputDto();
        response.setResultado(oopp.getResultado());
        response.setMensaje( oopp.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    
}
