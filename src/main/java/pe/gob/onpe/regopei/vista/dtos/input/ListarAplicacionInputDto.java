package pe.gob.onpe.regopei.vista.dtos.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListarAplicacionInputDto {
    private String nombre;
    private String descripcion;
    private String codigo;
    private String estado;
    private Integer idUnidadOrganica;
}
