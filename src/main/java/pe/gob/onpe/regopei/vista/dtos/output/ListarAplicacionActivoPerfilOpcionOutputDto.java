package pe.gob.onpe.regopei.vista.dtos.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarAplicacionActivoPerfilOpcionOutputDto {
    private Integer idAplicacion;
    private String nombre;
    private String descripcion;
    private String codigo;
    private String estado;
    private String url;
}
