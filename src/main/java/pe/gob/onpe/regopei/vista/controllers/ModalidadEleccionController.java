package pe.gob.onpe.regopei.vista.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import pe.gob.onpe.regopei.negocio.modelos.CabRegistroModalidadEleccion;
import pe.gob.onpe.regopei.negocio.modelos.Oopp;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.CabRegistroModalidadEleccionServicio;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.DetRegistroModalidadEleccionService;
import pe.gob.onpe.regopei.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.vista.dtos.input.oopp.RegistrarModalidadEleccionInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.CabRegistroModalidadEleccionOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.DatosCabRegistroModalidadEleccionOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.DatosRegistrarModalidadEleccionOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.RegistrarModalidadEleccionOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ResponseCabRegistroModalidadEleccionOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ResponseRegistroModalidadEleccionOutputDto;
import pe.gob.onpe.regopei.vista.dtos.wrapper.CabModalidadEleccionWrapper;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/modalidad-eleccion")
@Validated
public class ModalidadEleccionController {
	
	 @Autowired
	 private JWTTokenProvider jwtTokenProvider;
	 
	 @Autowired
	 private DetRegistroModalidadEleccionService detService;
    
    @Autowired
    private CabRegistroModalidadEleccionServicio modalidadServicio;

    @ApiOperation(value = "Obtener modalidad de eleccion de un oopp", notes = "obtener modalidad de eleccion por oopp")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
   
    @GetMapping("/obtener-modalidad-eleccion/{idOopp}")
    public ResponseEntity<?> listarOoppPorUsuario(@PathVariable ("idOopp") Integer idOopp ) throws Exception {

    	Oopp oopp = new Oopp();
    	oopp.setId(idOopp);
    	
    	CabRegistroModalidadEleccion modalidadEleccion = new  CabRegistroModalidadEleccion();
    	modalidadEleccion.setOopp(oopp);
    	
    	modalidadServicio.obtenerPorOoop(modalidadEleccion);
    	
    	CabRegistroModalidadEleccionOutputDto out = CabModalidadEleccionWrapper.CabRegistroModalidadEleccionOutputDtoToModelWrapper(!modalidadEleccion.getLista().isEmpty() ? modalidadEleccion.getLista().get(0):null);
    	
        DatosCabRegistroModalidadEleccionOutputDto  datos = new DatosCabRegistroModalidadEleccionOutputDto();
        datos.setRegistro(out);
      

        ResponseCabRegistroModalidadEleccionOutputDto response = new ResponseCabRegistroModalidadEleccionOutputDto();
        response.setResultado(modalidadEleccion.getResultado());
        response.setMensaje( modalidadEleccion.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    
    @PostMapping("/guardar-modalidad-eleccion")
    public ResponseEntity<?> guardarModalidadEleccion( 
    		@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
    		@RequestBody RegistrarModalidadEleccionInputDto input ) throws Exception {
    	
    	String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        
    	CabRegistroModalidadEleccion out = CabModalidadEleccionWrapper.CabRegistroModalidadEleccionToModelWrapper(input);
    	out.setUsuarioCreacion(numeroDocumento);
    	
    	modalidadServicio.guardarActualizar(out,input );
    	
       	RegistrarModalidadEleccionOutputDto outPut = new RegistrarModalidadEleccionOutputDto();
    	outPut.setIdCabRegistroModalidadEleccion(out.getId());
    	
    	DatosRegistrarModalidadEleccionOutputDto datos = new DatosRegistrarModalidadEleccionOutputDto();
    	datos.setRegistro(outPut);
    	
    	ResponseRegistroModalidadEleccionOutputDto response = new ResponseRegistroModalidadEleccionOutputDto();
        response.setResultado(out.getResultado());
        response.setMensaje( out.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    
    
    
    

    
    
}
