package pe.gob.onpe.regopei.vista.dtos.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseOutputDto {
	private String mensaje;
	private Integer resultado;
}
