package pe.gob.onpe.regopei.vista.dtos.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EliminarAplicacionInputDto {
    @NotNull(message = "El campo id de aplicacion usuario no puede ser nulo")
    private Integer idAplicacionUsuario;
}
