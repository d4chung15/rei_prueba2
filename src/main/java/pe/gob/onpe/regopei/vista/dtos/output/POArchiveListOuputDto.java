package pe.gob.onpe.regopei.vista.dtos.output;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class POArchiveListOuputDto {
    private Integer id;
    private Integer genInfoId;
    private Integer docType;
    private String originalName;
    private Long size;
}
