package pe.gob.onpe.regopei.vista.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pe.gob.onpe.regopei.negocio.modelos.Aplicacion;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.AplicacionServicio;
import pe.gob.onpe.regopei.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.transversal.utilidades.Funciones;
import pe.gob.onpe.regopei.vista.dtos.input.*;
import pe.gob.onpe.regopei.vista.dtos.output.ListarAplicacionActivoPerfilOpcionOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.ListarAplicacionOutputDto;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/aplicacion")
@Validated
public class AplicacionController {
    @Autowired
    AplicacionServicio aplicativoServicio;

    @Autowired
    JWTTokenProvider jwtTokenProvider;


    public AplicacionController() {
        super();
        // TODO Auto-generated constructor stub
        Funciones.configuraAplicacionToListarAplicacionOutputDtoMapper();
    }

    @PostMapping("/listar-aplicacion")
    public ResponseEntity<?> listarAplicacion(@RequestBody ListarAplicacionInputDto paramInputDto) throws Exception {
        // Implement

        Aplicacion param = Funciones.map(paramInputDto, Aplicacion.class);
        //param.setUnidadOrganica(new UnidadOrganica());
        //.getUnidadOrganica().setIdUnidadOrganica(paramInputDto.getIdUnidadOrganica());
        //
        aplicativoServicio.listarAplicaciones(param);

        List<ListarAplicacionOutputDto> outputDto = Funciones.mapAll(param.getAplicaciones(),
                ListarAplicacionOutputDto.class);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());
        resultado.put("lista", outputDto);

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @GetMapping("/listar-aplicacion-activo")
    public ResponseEntity<?> listarAplicacionActivo() throws Exception {
        // Implement
        Aplicacion param = new Aplicacion();
        aplicativoServicio.listarAplicacionesActivos(param);

        List<ListarAplicacionOutputDto> outputDto = Funciones.mapAll(param.getAplicaciones(), ListarAplicacionOutputDto.class);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());
        resultado.put("lista", outputDto);

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PostMapping("/registrar-aplicacion")
    public ResponseEntity<?> registrarAplicacion(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
                                                 @RequestBody @Valid final RegistrarAplicacionInputDto paramInputDto) throws Exception {
        // Implement
        String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        Aplicacion param = Funciones.map(paramInputDto, Aplicacion.class);
        param.setUsuarioCreacion(numeroDocumento);
        //param.setUnidadOrganica(new UnidadOrganica());
        //param.getUnidadOrganica().setIdUnidadOrganica(paramInputDto.getIdUnidadOrganica());

        aplicativoServicio.registrarAplicacion(param);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());
        resultado.put("idAplicacion", param.getIdAplicacion());

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PostMapping("/eliminar-aplicacion")
    public ResponseEntity<?> eliminarAplicacion(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
                                                @RequestBody EliminarAplicacionInputDto paramInputDto) throws Exception {
        // Implement

        String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        Aplicacion param = Funciones.map(paramInputDto, Aplicacion.class);
        param.setUsuarioModificacion(numeroDocumento);
        aplicativoServicio.eliminarAplicacion(param);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PostMapping("/actualizar-aplicacion")
    public ResponseEntity<?> actualizarAplicacion(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
                                                  @RequestBody ActualizarAplicacionInputDto paramInputDto) throws Exception {

        String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        Aplicacion param = Funciones.map(paramInputDto, Aplicacion.class);
        param.setUsuarioModificacion(numeroDocumento);
        //param.setUnidadOrganica(new UnidadOrganica());
        //param.getUnidadOrganica().setIdUnidadOrganica(paramInputDto.getIdUnidadOrganica());

        aplicativoServicio.actualizarAplicacion(param);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PostMapping("/actualizar-estado-aplicacion")
    public ResponseEntity<?> actualizarEstadoAplicacion(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
                                                        @RequestBody ActualizarEstadoAplicacionInputDto paramInputDto) throws Exception {
        // Implement
        String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        Aplicacion param = Funciones.map(paramInputDto, Aplicacion.class);
        param.setUsuarioModificacion(numeroDocumento);
        aplicativoServicio.actualizarEstadoAplicacion(param);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @GetMapping("/listar-aplicacion-activo-perfil-opcion")
    public ResponseEntity<?> listarAplicacionActivoPerfilOpcion() throws Exception {
        // Implement
        Aplicacion param = new Aplicacion();
        aplicativoServicio.listarAplicacionesActivosPorPerfilOpcion(param);

        List<ListarAplicacionActivoPerfilOpcionOutputDto> outputDto = Funciones.mapAll(param.getAplicaciones(),
                ListarAplicacionActivoPerfilOpcionOutputDto.class);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());
        resultado.put("lista", outputDto);

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }
}
