package pe.gob.onpe.regopei.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosConsultaPadronOutputDto {
	
	private ConsultaPadronNombreOutputDto persona;

}
