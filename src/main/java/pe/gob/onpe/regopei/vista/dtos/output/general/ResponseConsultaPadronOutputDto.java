package pe.gob.onpe.regopei.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.regopei.vista.dtos.output.BaseOutputDto;


@Getter
@Setter
public class ResponseConsultaPadronOutputDto extends BaseOutputDto {
	
	private DatosConsultaPadronOutputDto datos;

}
