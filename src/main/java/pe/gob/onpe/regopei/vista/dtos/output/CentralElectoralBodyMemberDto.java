package pe.gob.onpe.regopei.vista.dtos.output;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CentralElectoralBodyMemberDto {
    private String dni;
    private String firstSurname;
    private String secondSurname;
    private String names;
    private Integer positionId;
    private String position;
    private Integer status;
    private String usuarioCreacion;
}
