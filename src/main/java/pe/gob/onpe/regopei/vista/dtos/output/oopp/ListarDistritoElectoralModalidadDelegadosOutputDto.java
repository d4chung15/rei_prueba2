package pe.gob.onpe.regopei.vista.dtos.output.oopp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarDistritoElectoralModalidadDelegadosOutputDto {
	
	private Integer idDistritoElectoral;
	private Integer idDistritoElectoralPadre;
	private String 	nombreDistritoElectoral;
	private Integer cantidadDelegados;
	

}
