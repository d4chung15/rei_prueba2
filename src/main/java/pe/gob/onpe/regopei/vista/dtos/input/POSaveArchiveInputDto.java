package pe.gob.onpe.regopei.vista.dtos.input;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class POSaveArchiveInputDto {
    private Integer poId;
    private Integer docType;
    private String originalFilename;
    private String filename;
    private String path;
    private Long size;
    private String filetype;
    
    private String usuarioCreacion;
}
