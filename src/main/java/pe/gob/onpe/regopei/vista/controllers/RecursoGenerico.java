package pe.gob.onpe.regopei.vista.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.transversal.properties.CaptchaProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author glennlq
 * @created 11/05/21
 */
//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/recursogenerico")
@Validated
public class RecursoGenerico {

    @Autowired
    protected CaptchaProperties captchaSettings;

    @GetMapping(value="/apigoogle", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map apiGoogle() {
        Map<String, String> res = new HashMap<>();
        res.put("api", captchaSettings.getSite());
        return res;
    }

}
