package pe.gob.onpe.regopei.vista.dtos.input.oopp;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ListarDistritoElectoralModalidadDelegadosOutputDto;

@Getter
@Setter
public class RegistrarModalidadEleccionInputDto {
	
	private Integer idOopp;
	private Integer codigoModalidad;
	private Integer codigoTipoPresentacion;
	private Integer codigoModalidadCircunscripcionD;
	private Integer codigoFormaPresentacion;
	private Integer cantidadDelegados;
	private Integer idDistritoElectoral;
	
	private List<ListarDistritoElectoralModalidadDelegadosOutputDto> lista ;
	
	

}
