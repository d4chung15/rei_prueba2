package pe.gob.onpe.regopei.vista.dtos.wrapper;

import java.util.ArrayList;
import java.util.List;

import pe.gob.onpe.regopei.negocio.modelos.Oopp;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ListarOoppUsuarioOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ObtenerOoppPorIdOutputDto;

public class OoppWrapper {
	
	public static List<ListarOoppUsuarioOutputDto> ListarOoppUsuarioOutputDtoToModelWrapper(List<Oopp> origen) {
		
		if(origen == null) {
			return null;
		}
		
		List<ListarOoppUsuarioOutputDto> lista = new ArrayList<>();
		
		for (Oopp param : origen) {
			ListarOoppUsuarioOutputDto out = new ListarOoppUsuarioOutputDto();
			out.setId(param.getId());
			out.setNombreOopp(param.getNombreOopp());
			out.setNombreTipo(param.getTipoOopp().getNombre());
			out.setRegion(param.getDistritoElectoral().getNombre());
			lista.add(out);
			
		}
		
		return lista;
	}
	
	
	public static ObtenerOoppPorIdOutputDto ObtenerOoppPorIdOutputDtoToModelWrapper(Oopp origen) {
		if(origen == null) {
			return null;
		}
		
		ObtenerOoppPorIdOutputDto dto = new ObtenerOoppPorIdOutputDto();
		dto.setId(origen.getId());
		dto.setNombreOopp(origen.getNombreOopp());
		dto.setNombreTipo(origen.getTipoOopp().getNombre());
		dto.setTipo(origen.getTipoOopp().getCodigo());
		if(origen.getDistritoElectoral() != null) {
			dto.setIdDistritoElectoral(origen.getDistritoElectoral().getId());
			dto.setRegion(origen.getDistritoElectoral().getNombre());
			
		}
		
		return dto;
	}

}
