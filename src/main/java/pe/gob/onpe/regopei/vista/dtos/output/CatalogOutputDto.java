package pe.gob.onpe.regopei.vista.dtos.output;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CatalogOutputDto {
    private Integer id;
    private String name;
    private Integer parent;

    private List<CatalogDetailOutputDto> options;
}
