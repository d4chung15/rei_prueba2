package pe.gob.onpe.regopei.vista.dtos.input;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class SaveLegalRepresentativeInputDto {
    private String dni;
    private String name;
    private String email;
    private String cellphone;
    private String phone;
    private String address;
}
