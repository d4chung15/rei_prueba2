package pe.gob.onpe.regopei.vista.dtos.output;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.onpe.regopei.negocio.modelos.CElectoral;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CentralElectoralBodyMembersOutputDto {
    private List<CElectoral> members = new ArrayList();
}
