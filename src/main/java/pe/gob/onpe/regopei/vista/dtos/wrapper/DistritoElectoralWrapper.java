package pe.gob.onpe.regopei.vista.dtos.wrapper;

import java.util.ArrayList;
import java.util.List;

import pe.gob.onpe.regopei.negocio.modelos.DistritoElectoral;
import pe.gob.onpe.regopei.vista.dtos.output.oopp.ListarDistritoElectoralModalidadDelegadosOutputDto;

public class DistritoElectoralWrapper {
	
	public static List<ListarDistritoElectoralModalidadDelegadosOutputDto>ListarDistritoElectoralModalidadDelegadosOutputDtoToModelWrapper(List<DistritoElectoral> origen) {
		
		if(origen == null) {
			return null;
		}
		
		List<ListarDistritoElectoralModalidadDelegadosOutputDto> lista = new ArrayList<>();
		
		for (DistritoElectoral param : origen) {
			ListarDistritoElectoralModalidadDelegadosOutputDto out = new ListarDistritoElectoralModalidadDelegadosOutputDto();
			out.setIdDistritoElectoral(param.getId());
			out.setIdDistritoElectoralPadre(param.getIdPadre());
			out.setNombreDistritoElectoral(param.getNombre());
			out.setCantidadDelegados(param.getCantidadDelegados());
			lista.add(out);
			
		}
		
		return lista;
	}
	
	
	

}
