package pe.gob.onpe.regopei.vista.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.CatalogService;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.vista.dtos.output.CatalogOutputDto;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/catalog")
@Validated
public class CatalogController {

    @Autowired
    private CatalogService service;

    @GetMapping("/{name}")
    public ResponseEntity<CatalogOutputDto> getCatalog(@PathVariable String name) throws Exception {
        CatalogOutputDto ans = service.findCatalogByMaestro(name);
        return new ResponseEntity<>(ans, HttpStatus.OK);
    }
}
