package pe.gob.onpe.regopei.vista.dtos.output;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PoliticalOrganizationStep1OutputDto {
    private Integer type;
    private String dni;
    private String name;
    private String email;
    private String cellphone;
    private String phone;
    private String address;
}
