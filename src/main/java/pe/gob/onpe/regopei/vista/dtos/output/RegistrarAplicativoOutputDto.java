package pe.gob.onpe.regopei.vista.dtos.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarAplicativoOutputDto {
    private String descripcion;
    private String nombre;
}
