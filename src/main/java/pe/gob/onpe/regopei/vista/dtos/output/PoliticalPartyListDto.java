package pe.gob.onpe.regopei.vista.dtos.output;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PoliticalPartyListDto {
    private ArrayList<SimplePoliticalPartyDto> politicalParties = new ArrayList<>();
}
