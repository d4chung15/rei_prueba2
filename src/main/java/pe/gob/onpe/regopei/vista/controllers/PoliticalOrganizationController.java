package pe.gob.onpe.regopei.vista.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.onpe.regopei.negocio.modelos.GeneralInfoArchive;
import pe.gob.onpe.regopei.negocio.modelos.PORepresentative;
import pe.gob.onpe.regopei.negocio.modelos.PoliticalOrganization;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.PoliticalOrganizationService;
import pe.gob.onpe.regopei.negocio.servicios.interfaces.SftpService;
import pe.gob.onpe.regopei.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.regopei.transversal.constantes.ConstanteApi;
import pe.gob.onpe.regopei.vista.dtos.input.POSaveArchiveInputDto;
import pe.gob.onpe.regopei.vista.dtos.input.SaveLegalRepresentativeInputDto;
import pe.gob.onpe.regopei.vista.dtos.output.CentralElectoralBodyMemberDto;
import pe.gob.onpe.regopei.vista.dtos.output.CentralElectoralBodyMembersOutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.PoliticalOrganizationStep1OutputDto;
import pe.gob.onpe.regopei.vista.dtos.output.PoliticalPartyListDto;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/political-organization")
@Validated
public class PoliticalOrganizationController {

    @Autowired
    private PoliticalOrganizationService service;
    
    @Autowired
	 private JWTTokenProvider jwtTokenProvider;

    @Autowired
    private SftpService sftpService;

    @GetMapping("/my-list")
    public ResponseEntity<PoliticalPartyListDto> getMyList() {
        return null;
    }

    @GetMapping("/count-padron-rop")
    public ResponseEntity<Integer> countPadronRop() throws Exception {
        Integer cnt = service.countPadronRop();
        return new ResponseEntity<>(cnt, HttpStatus.OK);
    }

    @GetMapping("/{id}/general-information")
    public ResponseEntity<PoliticalOrganizationStep1OutputDto> getStep1Info(@PathVariable Integer id) throws Exception {
        PoliticalOrganizationStep1OutputDto ans = new PoliticalOrganizationStep1OutputDto();
        PoliticalOrganization po = service.getPoliticalOrganization(id);
        PORepresentative r = service.getPoliticalOrganizationInfo(id);

        ans.setType(po.getType().getCodigo());
        ans.setName(r.getName());
        ans.setDni(r.getDni());
        ans.setCellphone(r.getCellphone());
        ans.setEmail(r.getEmail());
        ans.setPhone(r.getPhone());
        ans.setAddress(r.getAddress());

        return new ResponseEntity<>(ans, HttpStatus.OK);
    }

    @PostMapping("/{id}/legal-representative")
    public ResponseEntity<Boolean> saveLegalRepresentative(@PathVariable Integer id, 
    		@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
    		@RequestBody SaveLegalRepresentativeInputDto input) throws Exception {
        if (!input.getDni().matches("^\\d{8}$")
                || input.getCellphone().length() > 20
                || input.getEmail().length() > 300
                || input.getPhone().length() > 20
                || input.getAddress().length() > 500) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        
        PORepresentative poRepresentative = new PORepresentative();
        poRepresentative.setPoId(id);
        poRepresentative.setDni(input.getDni());
        poRepresentative.setName(input.getName());
        poRepresentative.setCellphone(input.getCellphone());
        poRepresentative.setPhone(input.getPhone());
        poRepresentative.setAddress(input.getAddress());
        poRepresentative.setEmail(input.getEmail());
        poRepresentative.setUsuarioCreacion(numeroDocumento);
        service.savePoliticalOrganizationRepresentative(poRepresentative);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @GetMapping("/{id}/central-electoral-body-member")
    public ResponseEntity<CentralElectoralBodyMembersOutputDto> listCentralElectoralBodyMembers(@PathVariable Integer id) throws Exception {
        return new ResponseEntity<>(service.listCElectoralMembers(id), HttpStatus.OK);
    }

    @PostMapping("/{id}/central-electoral-body-member")
    public ResponseEntity<Boolean> addCentralElectoralBodyMember(@PathVariable Integer id,
    		@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
    		@RequestBody CentralElectoralBodyMemberDto member) throws Exception {
    	String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
    	member.setUsuarioCreacion(numeroDocumento);
        service.saveCElectoralBodyMember(id, member);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/central-electoral-body-member/{dni}")
    public ResponseEntity<Boolean> removeCentralElectoralBodyMember(@PathVariable Integer id, @PathVariable String dni) throws Exception {
        service.removeCElectoralMember(id, dni);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @PostMapping("/{id}/validate-central-electoral-body-members")
    public ResponseEntity<Boolean> validateDnis(@PathVariable Integer id) {
        return null;
    }

    @PostMapping("/{id}/upload-file/{type}")
    public ResponseEntity<Boolean> uploadFile(
    		@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
    		@PathVariable("id") Integer id, @PathVariable("type") Integer type,
                                        @RequestParam("filename") String filename,
                                        @RequestParam("size") Long size,
                                        @RequestParam("filetype") String filetype,
                                        @RequestParam("file") MultipartFile file) throws Exception {
    	String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        String newFilename = UUID.randomUUID() + "-" + filename;
        String filePath = "rei-documents/" + id + "/" + type + "/"  + newFilename;
        if (!sftpService.uploadFile(filePath, file.getInputStream())) {
            return ResponseEntity.status(500).body(false);
        }

        POSaveArchiveInputDto inputDto = POSaveArchiveInputDto.builder()
                .poId(id)
                .docType(type)
                .filename(newFilename)
                .path(filePath)
                .originalFilename(filename)
                .filetype(filetype)
                .size(size)
                .usuarioCreacion(numeroDocumento)
                .build();
     
        service.saveArchive(inputDto);

        return ResponseEntity.ok().body(true);
    }

    @GetMapping("/{id}/uploaded-files")
    public ResponseEntity<?> listFiles(@PathVariable("id") Integer id) throws Exception {
        return ResponseEntity.ok(service.listArchives(id));
    }

    @GetMapping(path = "/{id}/download-file/{fileId}")
    public void download(@PathVariable("id") Integer id,
                                      @PathVariable("fileId") Integer fileId,
                                      HttpServletResponse response) throws Exception {

        GeneralInfoArchive archive = service.getArchivesByGI(fileId);

        response.setHeader("Content-Type", archive.getArchive().getFormat());
        response.setHeader("Content-Disposition", "attachment; filename=" + archive.getArchive().getOriginalFilename() + ";");
        sftpService.downloadFile(archive.getArchive().getPath(), response.getOutputStream());
    }

    @DeleteMapping(path = "/{id}/download-file/{fileId}")
    public ResponseEntity<Boolean> deleteFile(@PathVariable("id") Integer id,
                                      @PathVariable("fileId") Integer fileId) throws Exception {

        service.removeFileByGI(fileId);
        return ResponseEntity.ok(true);
    }
}
