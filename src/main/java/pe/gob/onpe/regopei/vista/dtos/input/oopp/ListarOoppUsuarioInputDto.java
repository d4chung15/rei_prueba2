package pe.gob.onpe.regopei.vista.dtos.input.oopp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarOoppUsuarioInputDto   {
	
	private String usuario;
	private String perfil;
	private String nombreOopp;
	private Integer pagina;
	private Integer totalRegistroPorPagina;
	

}
