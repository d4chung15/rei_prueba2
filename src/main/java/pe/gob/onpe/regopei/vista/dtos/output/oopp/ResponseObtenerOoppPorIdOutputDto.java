package pe.gob.onpe.regopei.vista.dtos.output.oopp;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.regopei.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseObtenerOoppPorIdOutputDto extends BaseOutputDto{
	
	private DatosObtenerOoppPorIdOutputDto datos;

}
